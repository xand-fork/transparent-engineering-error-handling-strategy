pub mod confidentiality;
#[cfg(feature = "substrate")]
pub mod substrate;
pub mod transaction;
pub mod validator_emissions;

use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

// When defining error subdomains (wrappers around other error types) it is idiomatic to retain the 'Error` suffix.
#[allow(clippy::pub_enum_variant_names)]
#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    TransactionError(#[from] transaction::Error),
    #[error(transparent)]
    ConfidentialityError(#[from] confidentiality::Error),
    #[error(transparent)]
    ValidatorEmissionsError(#[from] validator_emissions::Error),
}
