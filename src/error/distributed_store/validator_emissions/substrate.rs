use super::Error as ValidatorEmissionsError;
use crate::traits::error_ext;

impl<R: Debug> error_ext::Substrate<R> for ValidatorEmissionsError {
    type Error = ValidatorEmissionsFailure<R>;

    fn as_substrate_error(&self) -> ValidatorEmissionsFailure<R> {
        match self {
            ValidatorEmissionsError::IdNotInAuthoritySet(user_id) => {
                crate::logging::log_information_before_data_loss(user_id);
                ValidatorEmissionsFailure::IdNotInAuthoritySet
            }
        }
    }
}

// -------------
// Ignore: The following dummy types exist only to make the demo compile.  In practice these types would be defined
// elsewhere in the project.
use std::{fmt::Debug, marker::PhantomData};
use thiserror::Error;

#[derive(Debug, Error)]
#[error("Dummy type")]
pub enum ValidatorEmissionsFailure<R: Debug> {
    IdNotInAuthoritySet,
    _PhantomData(PhantomData<R>),
}
impl<R: Debug> From<ValidatorEmissionsFailure<R>>
    for crate::error::distributed_store::substrate::DispatchError
{
    fn from(_: ValidatorEmissionsFailure<R>) -> Self {
        Self
    }
}
