use std::{
    fmt::{Display, Formatter, Result as IoResult},
    io::Error as IoError,
};
use thiserror::Error;

#[derive(Debug, Error)]
pub struct Error(IoError);

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> IoResult {
        write!(f, "{}", self.0.to_string())
    }
}

impl From<std::io::Error> for crate::error::Error {
    fn from(err: std::io::Error) -> Self {
        Error(err).into()
    }
}
