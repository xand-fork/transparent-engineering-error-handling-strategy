pub mod distributed_store;
pub mod io;

use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    IoError(#[from] io::Error),
    #[error(transparent)]
    DistributedStoreError(#[from] distributed_store::Error),
}

#[allow(non_snake_case)]
#[cfg(test)]
mod test {
    use super::*;

    fn store_confidentiality_failure() -> Result<()> {
        let txn_err: distributed_store::Error =
            distributed_store::confidentiality::Error::CreateAmountMustBeMoreThanZero(
                distributed_store::confidentiality::Balance {},
            )
            .into();
        Err(txn_err.into())
    }

    #[test]
    fn error__match_easily_tested() {
        // when
        let received_error: Error = store_confidentiality_failure().unwrap_err();

        // then
        assert_matches::assert_matches!(
            received_error,
            Error::DistributedStoreError(distributed_store::Error::ConfidentialityError(
                distributed_store::confidentiality::Error::CreateAmountMustBeMoreThanZero(
                    distributed_store::confidentiality::Balance {}
                )
            ))
        );
    }
}
