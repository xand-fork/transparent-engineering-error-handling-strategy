use std::fmt::Debug;

pub fn log_information_before_data_loss<T: Debug>(data: T) {
    // Here is where you would do any necessary logging of data that would otherwise be lost during translation
    println!("[LOG]:: Data about to be lost: {:?}", data);
}
