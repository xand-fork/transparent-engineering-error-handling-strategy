# Transparent Engineering Error Handling Strategy

## Overview

This project contains documentation and an example implementation for error handling in Rust crates at Transparent.  The motivation is to provide an error-handling strategy whose primary goals are ease-of-maintenance and full-fidelity error handling and reporting. The goal is simple:  Use a fully expressive (i.e. error types with context) canonical error set that is specific to the domain of a crate wherever possible.  Cases such as Substrate whose error type does not permit context might at first seem to be irreconcilable with this goal, but the example code shows how this can be managed while fully sandboxing dependency's (Substrate) "lowest-common-deniminator" error handling strategy so that it does not leak into our code.

The example code in this repository is located under the `/src` directory and is broken into a top-level domain, and two sub-domains:
1. [The top level hierarchy (`crate::error`)](./src/error.rs) represents the complete domain of errors for a given crate.  It shows examples of translating a canonical error to a different structure using Rust-standard `From` impls (provided by `thiserror` to reduce boilerplate).
1. [The first sub-domain (`io`)](./src/error/io.rs) shows an example of working with "difficult" `std` errors--errors that do not implement key traits necessary to make testing straightforward. For example, `std::io::Error` does not implement `PartialEq` and as a result, cannot be compared with another instance of the same type.  When included in a `Result`, it is viral, making `Results` also incomparable.  Tests become more verbose and difficult to write, when a simple `assert!(actual == expected)` or `assert_eq!(actual, expected)` should suffice.
1. [The second sub-domain (`distributed_store`)](./src/error/distributed_store.rs) shows the fractal nature of this pattern where it itself is comprised of three (sub-)sub-domains, demonstrating that the pattern lends itself to an arbitrary level of granularity, depending on the situation by simply repeating the technique(s) employed at the root level.
    - `distributed_store` is broken into sub domains:
        1. [`transaction`](./src/error/distributed_store/transaction.rs)
        1. [`confidentiality`](./src/error/distributed_store/confidentiality.rs)
        1. [`validator_emissions`](./src/error/distributed_store/validator_emissions.rs)
1. [The `distributed_store` substrate adapter](./src/error/distributed_store/substrate.rs) shows an example of how implementation details of a domain (for example, using substrate for our distributed store vs. using sawtooth for our distributed store) can be abstracted safely from canonical errors and even on a per-feature basis.
    - `substrate` adapters are implemented for each subdomain of `distributed_store`:
        1. [`transaction` substrate adapter](./src/error/distributed_store/transaction/substrate.rs)
        1. [`confidentiality` substrate adapter](./src/error/distributed_store/confidentiality/substrate.rs)
        1. [`validator_emissions` substrate adapter](./src/error/distributed_store/validator_emissions/substrate.rs)

## Error Structure

The structure of an Error in a Transparent crate has only a few requirements:

- It must implement the `std::error:Error` trait
- It must be named `Error`
- It must have a `Result` structure for returns containing the `Error`
- It must not create any additional dependencies
- It must be *easily* testable

Naming the error set `Error` and the result `Result` is following the pattern laid out by the Rust standard library and relies on the module namespace to disambiguate one `Error` type from another (e.g. `std::fmt::Error` vs `std::convert::TryInto::Error`).  While the `Result` alias's error type may be overridden, implementers of a module should be aware that using multiple sets of errors in a single domain like this is likely a smell that suggests tight coupling, low cohesion and violation(s) of the single responsibility principle.

```rust
// `error.rs`
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Display implementation1: {0}")]
    Variant1(String),
    //...
    #[error("Display implementation2: {0} at {1}")]
    Variant2(usize, Path),
    },
}
```

```rust
// `lib.rs`
use crate::Result;

pub fn action_in_domain() -> Result<()> { todo!() }
```

## Maintaining Fidelity of Standard and Third-Party Errors in Canonical Error Sets

Sometimes `std` errors do not implement fully the `std::error::Error` trait or other traits that are required by the canonical error set.  In this case, you should implement the missing traits and wrap the error for use.  Since it is a `std` error, wrapping it fully does not create an additional dependency on consumers of the error set.  [The first sub-domain (`io`)](./src/error/io.rs) shows an example of working with "difficult" `std` errors.

Errors from third-party libraries must be handled a bit differently to maintain full fidelity of errors while preventing the creation of additional dependencies for the error set.  The best way to do this is to wrap the third party error in a canonical error as a `Box<dyn Error + Send + Sync>` payload.  This will allow all expected error handling without creating dependencies.

Some third-party errors do not implement `Error`, in which case it is appropriate to store a stringified version of the error as a payload.  To prevent consumers from needing knowledge of a third party error's `Error` implementation, an enum to wrap the propagated error should be implemented.

Also, if the third party error does not implement the standard error trait or `Send + Sync`, the best case scenario is to stringify the error via its `debug` (or `display`) implementation.

Additional payload fields beyond the boxed error could store relevant primitives if necessary, e.g. `Error(WrappedThirdPartyErrorEnum, String, uint32)`.

> **Note**: Why `Send + Sync`?
>
> Without `Send + Sync`, Errors sent over a thread will create the possibility of a compiler error.  This is especially a problem for async code where this can happen implicitly.  See https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=e6513a5c5a2343fb77c5dfb8b8708c05 for an example of the issue.

The [transactions subdomain error set](./src/error/distributed_store/transaction.rs) shows an example of using Boxed and stringified errors from third party libraries wrapped in a singular enum for consumers.

## Testing
Ease of use for errors, especially in testing, is necessary for quality maintenance.  The best way to compare errors without restrictive trait derives such as `PartialEq` is to use the [`assert_matches` crate](https://docs.rs/matches/0.1.8/matches/macro.assert_matches.html).  Using this crate will allow tests that check for expected error returns and still structure `Error` structs without compromises to fidelity of the error.  An example basic use can be seen in the [top-level error module](./src/errors.rs).

## Logging
When translating canonical errors to another type that may result in the loss of data, that data should be logged in some way to prevent it from being lost to future diagnostics.  An example of this is shown in the project's substrate error adapters in the distributed_store subdomains.

## Why `thiserror`?
The best way to implement errors in our ecosystem is the `thiserror` crate, which provides quality-of-life syntax for implementing traits such as `From` and `Display`.

Rust has a long history over many years with error type libraries.  Briefly, a good error handling library is designed to remove boilerplate, improve expressiveness and _leave no trace of itself in the code it generates_.  David Tolnay's `thiserror` and its sister crate `anyhow` is, by far, the most popular (and may even be the only) error handling crates available to meet all these criteria.  Specifically, `snafu`, `failure`, `fehler`, `quick-error` and `error-chain` all fail to meet this criteria and should be avoided for future development wherever possible.

Note `thiserror` is designed to streamline error definitions for libraries while `anyhow` is designed for applications--both are from the same author and are designed to interoperate when needed.
